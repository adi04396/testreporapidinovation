import React, { Component } from 'react'
import { HashRouter, Route, Switch, Redirect } from 'react-router-dom'
import './scss/style.scss';
import "antd/dist/antd.css";

const loading = (
  <div className="pt-3 text-center">
    <div className="sk-spinner sk-spinner-pulse"></div>
  </div>
)

// Containers
const DefaultLayout = React.lazy(() => import('./layout/DefaultLayout'))

// Pages
const Login = React.lazy(() => import('./views/pages/login/Login'))
const Register = React.lazy(() => import('./views/pages/register/Register'))
const Page404 = React.lazy(() => import('./views/pages/page404/Page404'))
const Page500 = React.lazy(() => import('./views/pages/page500/Page500'))

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { isAuthenticaed: window.localStorage.getItem('authId') ? true : false };
  }
  

  render() {
    const {isAuthenticaed} = this.state;
    console.log(isAuthenticaed)
    return (
      <HashRouter>
        <React.Suspense fallback={loading}>
          <Switch> 
            {isAuthenticaed ? 
              <Route path="/dashboard" name="Home" render={(props) => <DefaultLayout {...props} />} /> 
            :
              <Route exact path="/login" name="Login Page" render={(props) => <Login {...props} />} />
            }
          </Switch>
        </React.Suspense>
      </HashRouter>
    )
  }
}

export default App
