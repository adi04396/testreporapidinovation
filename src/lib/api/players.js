import sendRequest from './sendRequest';

const BASE_PATH = 'api';

export const getplayers = data =>
    sendRequest(`${BASE_PATH}/players/getallPlayer?searchQuery=${data ? data?.searchQuery : ''}&authId=${data ? data?.authId : ''}`, {
        method: 'GET'
    });

export const savePlayer = data =>
    sendRequest(`${BASE_PATH}/players/addPlayer`, {
        body: JSON.stringify(data),
    });

// export const savePlayer = data =>
//     sendRequest(`${BASE_PATH}/players/addPlayer`, {
//         body: JSON.stringify(data),
//     });