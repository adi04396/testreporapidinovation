export default function getRootUrl() {
    const ROOT_URL = process.env.API_URL ? process.env.API_URL : 'http://localhost:5000/';
    return ROOT_URL;
}