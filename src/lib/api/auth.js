import sendRequest from './sendRequest';

const BASE_PATH = 'api';


export const authlogin = data => 
    sendRequest(`${BASE_PATH}/players/Login`, {
        body: JSON.stringify(data),
    });
