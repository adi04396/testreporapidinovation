import React, { useState, useEffect } from 'react';
import {Row, Col, Card, Table, Select, AutoComplete,Input, Tabs, Pagination,Button, Spin, Menu, Dropdown, Modal} from 'antd'; 
import AddTeamMember from './addTeamMemberModal';
import { getplayers } from '../../lib/api/players';
import {useHistory} from 'react-router-dom';

const Dashboard = () => {
  const Search = Input.Search;
  const [ismodalVisible, setIsmodalVisible] = useState(false);
  const [dataSource, setdataSource] = useState([]);
  let history = useHistory();

  useEffect(() => {
      _fetchplayes();
  }, []);

  const _fetchplayes = async () => {
    const searchData = {
      searchQuery: '',
      authId: window.localStorage.authId
    }
    let response = await getplayers(searchData);
    if(response && response.length > 0) {
      setdataSource(response);
    }
  }
  
  const columns = [
    {
      title: 'Player Name',
      dataIndex: 'player_name',
      key: 'player_name',
    },
    {
      title: 'Sport Type',
      dataIndex: 'sportType',
      key: 'sportType',
    },
    {
      title: 'Matches Played',
      dataIndex: 'matchesPlayed',
      key: 'matchesPlayed',
    },
    {
      title: 'Matches Won',
      dataIndex: 'matcheswon',
      key: 'matcheswon',
    },
    {
      title: 'Matches Lost',
      dataIndex: 'matcheslost',
      key: 'matcheslost',
    },
  ];

  const onSearch = async (val) => {
    const searchData = {
      searchQuery: val,
      authId: window.localStorage.authId
    }
    let response = await getplayers(searchData);
    if(response && response.length > 0) {
      setdataSource(response);
    }
  }
  
  return (
    <>
      <div className="containerNew scrollViewMain scrollDashboard DocumentsSponsorPage">
        <div className="profileTopBar">
            <div className="">
                <div className="searchDropDown">
                  <Row className="row">
                  <Col span={12}>&nbsp;</Col>
                    <Col span={12} className="col-md-12">  
                      <div className="autoCompleteSelectCont selectMidium DocumentSearch">
                        <Search
                            className="autoSearch"
                            placeholder="Search here"
                            onSearch={(val) => onSearch(val)}
                            style={{}}
                        />
                      </div>
                    </Col>
                  </Row>
                </div>
              </div>
              <Row>
                <Col span={24}>
                  <Card className="card cardWhite heightAuto transactionTable DocumentTable">
                    <Table columns={columns} dataSource={dataSource} />
                  </Card>
                </Col>
                <Col span={24}>
                {window.localStorage.getItem('authId') !== '1' && <div className="ButtonsAgGrid DocumentBtn"><Button onClick={() => setIsmodalVisible(!ismodalVisible)}>Add Team Member</Button></div>}
                </Col>
              </Row>
              </div>          
        </div>
         <AddTeamMember handleModal={() => setIsmodalVisible(!ismodalVisible)} ismodalVisible={ismodalVisible} _fetchplayes={_fetchplayes}/> 
    </>
  )
}

export default Dashboard
