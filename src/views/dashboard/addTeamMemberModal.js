import React from 'react';
import { Table, Tag, Space, Button, Modal, notification } from 'antd'; 
import { Formik,useFormik } from 'formik';
import {
    SubmitButton,
    Input,
    Form,
    FormItem,
  } from "formik-antd";
  import * as Yup from 'yup';
  import { savePlayer } from '../../lib/api/players';
  
  const AddTeamMember = (props) => {

    const addPlayerSchema = Yup.object().shape({
        player_name: Yup.string()
        .min(1, 'Too Short!')
        .required('Required!'),
        sportType: Yup.string()
        .min(1, 'Too Short!')
        .required('Required!'),
        matchesPlayed: Yup.number().required('Required!'),
        matcheswon: Yup.number().required('Required!'),
        matcheslost: Yup.number().required('Required!')
    });


    return (
        <Modal  destroyOnClose={true} title="Add an Team Member" centered visible={props && props?.ismodalVisible} onCancel={props && props?.handleModal} className="defaultForm ">
            <Formik
                enableReinitialize
                initialValues={{
                    player_name: '',
                    sportType: '',
                    matchesPlayed: '',
                    matcheswon: '',
                    matcheslost: '',
                    coachId: window.localStorage.authId 
                }}
                validationSchema={addPlayerSchema}
                onSubmit={async(value, {setFieldValue,resetForm}) => {
                    const response = await savePlayer(value);
                    if(response.status == '201') {
                        resetForm();
                        props?._fetchplayes();
                        notification.open({
                            message: 'Player Added Successfully',
                        });
                        return false;
                    } else {
                        resetForm();
                        notification.open({
                            message: 'Error in adding Player',
                        });
                        return false;
                    }
                }}
            >
            {({ values, errors, touched, setFieldValue }) =>
                <Form  name="addDependent"  className="defaultForm">
                    <FormItem label="Player Name *" name="player_name">
                        <Input name="player_name"  placeholder="Player Name" />
                    </FormItem>
                    <FormItem label="Sport Type *" name="sportType">
                        <Input name="sportType"  placeholder="Sport Type" />
                    </FormItem>
                    <FormItem label="Matches Played *" name="matchesPlayed">
                        <Input name="matchesPlayed" placeholder="Matches Played" />
                    </FormItem>
                    <FormItem label="Matches Won *" name="matcheswon">
                        <Input name="matcheswon" placeholder="Matches Won" />
                    </FormItem>
                    <FormItem label="Matches Lost *" name="matcheslost">
                        <Input name="matcheslost" placeholder="matches Lost" />
                    </FormItem>
                    <FormItem name="save" className="buttonRight addAdminBtns">
                        {/* <Button key="back" onClick={handleModal} className="CancelButtonrightadditional">Cancel</Button><span></span> */}
                        <SubmitButton > Save </SubmitButton>
                    </FormItem>
            </Form>
            }
            </Formik>
        </Modal>
    )
}

export default AddTeamMember;