const Players = require("../models/players.model.js");
const Users = require("../models/users.model.js");


//Validate login process
exports.login = (req, res) => {
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
  }

  const request = {
    email: req.body.email,
    password: req.body.password
  }

  Users.findByEmail(request, (err, data) => {
    if (err)
      res.status(500).send({
        status: 400,
        message:
          err.message || "User not found."
      });
    else res.send({
      status: 201,
      data: data });
  });

}


// Create and Save a new Players
exports.create = (req, res) => {
  // Validate request
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
  }
  // Create a Players
  const players = new Players({
    player_name: req.body.player_name,
    sportType: req.body.sportType,
    matchesPlayed: req.body.matchesPlayed,
    matcheswon: req.body.matcheswon,
    matcheslost: req.body.matcheslost,
    coachId: req.body.coachId
  });
  // Save Players in the database
  Players.create(players, (err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Players."
      });
    else res.send({
      status: 201,
      message:
        "Data Saved Successfully."
    });
  });
};

// Retrieve all Playerss from the database (with condition).
exports.findAll = (req, res) => {
  const searchData = {
    search: req.query.searchQuery,
    id: req.query.authId
  }

  Players.getAll(searchData, (err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving Playerss."
      });
    else res.send(data);
  });
};

