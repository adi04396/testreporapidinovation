const sql = require("./db.js");

// constructor
const Players = function(players) {
  this.player_name = players.player_name;
  this.matchesPlayed = players.matchesPlayed;
  this.sportType = players.sportType;
  this.matcheswon = players.matcheswon;
  this.matcheslost = players.matcheslost;
  this.coachId = players.coachId;

};

Players.create = (newPlayers, result) => {
  sql.query("INSERT INTO players SET ?", newPlayers, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    console.log("created Players: ", { id: res.insertId, ...newPlayers });
    result(null, { id: res.insertId, ...newPlayers });
  });
};


Players.getAll = (searchQuery, result) => {
  let query = "SELECT * FROM players";

  if (searchQuery.search) {
    query += ` WHERE player_name LIKE '%${searchQuery.search}%' OR sportType LIKE '%${searchQuery.search}%'`;
  }

  if(searchQuery.id != '1') {
    query += ` ${searchQuery.search ? 'AND' : 'WHERE'} coachId = '${searchQuery.id}'`;
  }

  query += ` ORDER BY matcheswon desc`;

  sql.query(query, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log("players: ", res);
    result(null, res);
  });
};

module.exports = Players;
