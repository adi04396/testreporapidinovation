module.exports = app => {
  const players = require("../controllers/players.controller.js");

  var router = require("express").Router();

  // Create a new Player
  router.post("/addPlayer", players.create);

  // Retrieve all Player
  router.get("/getallPlayer", players.findAll);

  router.post("/login", players.login);

  app.use('/api/players', router);
};
