-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 31, 2021 at 03:39 PM
-- Server version: 8.0.27-0ubuntu0.20.04.1
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `playerdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `players`
--

CREATE TABLE `players` (
  `id` int NOT NULL,
  `player_name` varchar(255) DEFAULT NULL,
  `sportType` varchar(255) DEFAULT NULL,
  `matchesPlayed` int DEFAULT NULL,
  `matcheswon` int DEFAULT NULL,
  `matcheslost` int DEFAULT NULL,
  `coachId` int DEFAULT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `players`
--

INSERT INTO `players` (`id`, `player_name`, `sportType`, `matchesPlayed`, `matcheswon`, `matcheslost`, `coachId`, `createdAt`, `updatedAt`) VALUES
(7, 'Rahul', 'Cricket', 10, 1, 1, 2, '2021-12-31 08:37:00', '2021-12-31 08:37:00'),
(8, 'Sharma', 'as', 2, 2, 2, 2, '2021-12-31 08:37:14', '2021-12-31 08:37:14'),
(9, 'wqeq', 'qweqw', 3, 2, 2, 2, '2021-12-31 08:37:29', '2021-12-31 08:37:29'),
(10, 'qweq', 'qweq', 1, 1, 1, 2, '2021-12-31 08:38:23', '2021-12-31 08:38:23'),
(11, 'aaa', 'aaa', 1, 1, 1, 2, '2021-12-31 08:38:33', '2021-12-31 08:38:33'),
(12, 'qwerty', 'Badminton', 1, 1, 1, 3, '2021-12-31 09:12:51', '2021-12-31 09:12:51'),
(13, 'we', 'qwe', 2, 22, 2, 3, '2021-12-31 09:13:05', '2021-12-31 09:13:05'),
(14, 'qwe', 'qweq', 1, 1, 1, 3, '2021-12-31 09:21:39', '2021-12-31 09:21:39'),
(15, 'Test', 'tabel tennis', 1, 1, 0, 2, '2021-12-31 09:57:39', '2021-12-31 09:57:39'),
(16, 'Test', 'Test', 1, 1, 1, 3, '2021-12-31 09:59:09', '2021-12-31 09:59:09'),
(17, 'Testt', 'Cricket', 1, 2, 1, 2, '2021-12-31 10:03:24', '2021-12-31 10:03:24');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int NOT NULL,
  `email` varchar(200) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `isadmin` tinyint NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `isadmin`, `createdAt`) VALUES
(1, 'admin@admin.com', '123456', 1, '2021-12-31 06:15:51'),
(2, 'coach@coach.com', '123456', 0, '2021-12-31 06:16:09'),
(3, 'coach1@gmail.com', '123456', 0, '2021-12-31 08:40:04');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `players`
--
ALTER TABLE `players`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `players`
--
ALTER TABLE `players`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
