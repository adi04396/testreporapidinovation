## Test App for Coach using React Node
# Process for SetUp
1. clone the project
2. Change the db config path -> /coreui-free-react-admin-template-main/app/config/db.config.js
3. Take the dump of mysql file playerdb.sql from root of project and import in mysql
4. npm install
5. npm run dev -> development server will start at 3000 and Node will run at 5000.

# Components and library description
1. http://localhost:3000/#/dashboard -> This is the dashboard
2. http://localhost:3000/#/login -> login page

# Credentials for the login
1. admin@admin.com/123456
2. coach@coach.com/123456
3. coach1@coach.com/123456

# Important
1. Pages might not load please refresh over the urls given Here is a recorded loom for more info:- https://www.loom.com/share/1919a32b323d40db8871632651847fde

Thanks!!